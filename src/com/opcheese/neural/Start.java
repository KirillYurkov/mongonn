package com.opcheese.neural;

import com.mongodb.*;
import org.jongo.Find;
import org.jongo.FindOne;
import org.jongo.Jongo;
import org.jongo.MongoCollection;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;

public class Start {
    public static void main(String[] args) {

        Mongo m = null;
        try {
            m = new Mongo("localhost", 27017);
        } catch (UnknownHostException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Jongo jongo = new Jongo(m.getDB("exhibits"));
        MongoCollection logRes = jongo.getCollection("LogRes");

        MongoCollection friends = jongo.getCollection("RhoRes");
        MongoCollection meta = jongo.getCollection("MetaStat");
        Iterable<MetaStat> as = meta.find("{$where:'this._id.length==14'}").as(MetaStat.class);
        long counter = meta.count("{$where:'this._id.length==14'}");

        int newOne = 0;


        int minSup = 5;
        ArrayList <CandidateSet> acs = new ArrayList<CandidateSet>();
        ArrayList<String> names = new ArrayList<String>();
        CandidateSet.names = names;
        CandidateSet.countNames = counter;
        counter = 0;
        for (MetaStat ms :as)
        {
            counter++;
            names.add(ms._id);
            CandidateSet cs = new CandidateSet();
            cs.bs.set((int)counter-1);
            cs.Support = (double)(Double)ms.value.get("count");
            if (cs.Support>minSup)
            {
                acs.add(cs);
            }
        }

        int lengthF = 0;
        do
        {
            newOne = 0;
            lengthF++;
            int siz = acs.size()   ;
        for(int i=0;i<siz;i++)
        {
            for(int j=i+1;j<siz;j++)
            {
                if (acs.get(i).bs.cardinality()>=lengthF)
                {
                CandidateSet cs = CandidateSet.JoinSets(acs.get(i),acs.get(j));
               boolean copy = false;
                   for(int k=0;k<acs.size() ;k++)
                 {
                     if (acs.get(k).bs.equals(cs.bs))
                     {
                         copy = true;
                         break;
                     }
                  }
                               if (!copy)
                               {
                cs.newOne = true;
                acs.add  (cs);     }

                }
            }
        }

        //addsky ploho
        DBCollection col = jongo.getDatabase().getCollection("WiFiPatterns");
        BasicDBObject qb = new BasicDBObject();
        // qb.put(key, new BasicDBObject("$exists", true));
        DBCursor cursor = col.find(qb);

        while (cursor.hasNext()) {
            cursor.next();
            for(int i=acs.size()-1;i>=0;i--)
            {
               CandidateSet cs = acs.get(i);
                if (cs.newOne)
                {
                    boolean hasAll =true;
                     for (String ms:cs.GetNames())
                     {
                         if (!cursor.curr().containsField(ms))
                         {
                             hasAll = false;
                             break;
                         }

                     }
                    if (hasAll)
                    {
                        cs.Support++;
                    }
                }
            }
        }

        for(int i=acs.size()-1;i>=0;i--)
        {
            CandidateSet cs = acs.get(i);
           if (cs.Support<minSup)
           {
               acs.remove(i);
           }
            else
           {
               if ( cs.newOne)
               newOne++;
               cs.newOne = false;
           }
        }
        }
        while (newOne>0)  ;
        for(int i=acs.size()-1;i>=0;i--)
        {
            CandidateSet cs = acs.get(i);
            if (cs.bs.cardinality()<3)
            {
                acs.remove(i);
            }
        }
        Collections.sort(acs,new Comparator<CandidateSet>() {
            @Override
            public int compare(CandidateSet o1, CandidateSet o2) {
                if (o1.Support>o2.Support)
                {
                    return 1;
                }
                if (o1.Support==o2.Support)
                {
                    return 0;
                }
                if (o1.Support<o2.Support)
                {
                    return -1;
                }
                return 0;  //To change body of implemented methods use File | Settings | File Templates.
            }
        });
        for (CandidateSet cs : acs)
        {

            System.out.println(cs.toString());
        }



        // LoNNRes as = meta.findOne("{_id:'5057e551f9d57f7174aafc6c'}").as(LoNNRes.class);
        //as.Func
        //-87



//        for(DBObject rhor:dbObjects)
//		{
//			if (rhor.get("_id").toString().length()==14)
//            {
//                Find cursor = friends.find("{ '$or' : [{ '_id.z1' : # }, { '_id.z2' : # }] })",rhor.get("_id").toString(),rhor.get("_id").toString()).sort("{ 'value.rho' : -1 }");
//                proc(cursor.as(Rhory.class), jongo, rhor.get("_id").toString(),logRes);
//            }
//
//		}


        //proc();
      //  proc(cursor.as(Rhory.class), jongo, "00:19:2f:32:af",logRes);
//	     
//		Iterable<Rhory> ic = cursor.as(Rhory.class);
//		Rhory c = null;
//		for(Rhory rhor:ic)
//		{
//			System.out.println(rhor.value.rho);
//			 mmp.put(rhor.value.rho, rhor);
//		}
//		for(double rho:mmp.keys())
//		{
//			 System.out.println(rho);
//		}

    }

    public void proc5()
    {

    }

    public static void proc4(MongoCollection mcol) {
        Perceptron perceptron = new Perceptron(3, 2);

        mcol.save(perceptron);


    }

    public static void proc(Iterable<Rhory> ic, Jongo jongo, String key, MongoCollection logRes) {
        int i = 0;
        Vector<String> ks = new Vector<String>();
        int kol = 1;
        for (Rhory rhor : ic) {
            ks = new Vector<String>();

            String tempRes;
            if (rhor._id.z1.equals(key)) {
                tempRes = rhor._id.z2;
            } else {
                tempRes = rhor._id.z1;
            }
          //  if (i > 0)
                ks.add(tempRes);
            i++;


        //kol = kol - 1;
//		MongoCollection friends = jongo.getCollection("WiFiPatterns");
//		friends.find("{'00:19:2f:32:af':{$exists:true}}").;
        DBCollection col = jongo.getDatabase().getCollection("WiFiPatterns");
        BasicDBObject qb = new BasicDBObject();
       // qb.put(key, new BasicDBObject("$exists", true));
        DBCursor cursor = col.find(qb);
        Vector<double[]> vyb = new Vector<double[]>();
        int cou = 0;
        int couNew = 0;

        boolean allZero = false;

        while (cursor.hasNext()) {
            int zeroCou = 0;
            cou++;
            cursor.next();
            double[] inp = new double[kol + 1];
            int j = 0;
            for (String vec : ks) {

                if (cursor.curr().containsField(vec)) {
                    inp[j] = -(double) (Double) cursor.curr().get(vec);

                } else {
                    inp[j] = 0.0;
                    zeroCou++;
                }
                j++;
            }

            if (cursor.curr().containsField(key)) {
                inp[j] = -(double) (Double) cursor.curr().get(key);
                if (inp[j] == 0) {
                    zeroCou++;
                }
            } else {
                inp[j] = 0.0;
            }

            if (!allZero || zeroCou < kol) {
                vyb.add(inp);
            }
            if (zeroCou == kol) {
                allZero = true;
            }
        }
        cursor.close();

//        qb = new BasicDBObject();
//        qb.put(key, new BasicDBObject("$exists", false));
//        cursor = col.find(qb);
//        allZero = false;
//        while (cursor.hasNext() && couNew < cou) {
//            int zeroCou = 0;
//            couNew++;
//            cursor.next();
//            double[] inp = new double[kol + 1];
//            int j = 0;
//            for (String vec : ks) {
//                if (cursor.curr().containsField(vec)) {
//                    inp[j] = -(double) (Double) cursor.curr().get(vec);
//                    if (inp[j] == 0) {
//                        zeroCou++;
//                    }
//                } else {
//                    inp[j] = 0.0;
//                    zeroCou++;
//                }
//                j++;
//            }
//            if (cursor.curr().containsField(key)) {
//                inp[j] = -(double) (Double) cursor.curr().get(key);
//
//            } else {
//                inp[j] = 0.0;
//
//            }
//            if (!allZero || zeroCou < kol) {
//                vyb.add(inp);
//            }
//            if (zeroCou == kol) {
//                allZero = true;
//            }
//        }

        for (double[] vd : vyb) {
            for (double d : vd) {
                System.out.print(d);
                System.out.print(" ");
            }
            System.out.println();
        }

        Perceptron p = new Perceptron(kol, 1);
        p.magicNumberFunction =1;
        double[][] vybMas = new double[vyb.size()][];
        vybMas = vyb.toArray(vybMas);
        double[][] trueMas = new double[vybMas.length][];
        for (i = 0; i < vybMas.length; i++) {
            for (int j = 0; j < kol; j++) {
                vybMas[i][j] = vybMas[i][j] / 100.0;
            }
            trueMas[i] = new double[1];
            trueMas[i][0] = vybMas[i][kol] / 100.0;
        }

        double res = p.Learn((vybMas), trueMas, 40);
            LoNNRes lres = new LoNNRes();
            lres.CountError=res;
            lres.DepVar = key;
            lres.IndVar =    tempRes;
            lres.Func = p;
            logRes.save(lres);
        }
    }
    public static void proc2(Iterable<Rhory> ic, Jongo jongo, String key) {
        int i = 0;
        Vector<String> ks = new Vector<String>();
        int kol = 1;
        for (Rhory rhor : ic) {
            String tempRes;
            if (rhor._id.z1.equals(key)) {
                tempRes = rhor._id.z2;
            } else {
                tempRes = rhor._id.z1;
            }
            //if (i>0)
            ks.add(tempRes);
            i++;
            if (i >= kol) {
                break;
            }
        }
        //kol =kol-1;
//		MongoCollection friends = jongo.getCollection("WiFiPatterns");
//		friends.find("{'00:19:2f:32:af':{$exists:true}}").;
        DBCollection col = jongo.getDatabase().getCollection("WiFiPatterns");
        BasicDBObject qb = new BasicDBObject();
        //qb.put(key,new BasicDBObject("$exists", true));
        DBCursor cursor = col.find(qb);
        Vector<double[]> vyb = new Vector<double[]>();
        int cou = 0;
        int couNew = 0;

        boolean allZero = false;

        while (cursor.hasNext()) {
            int zeroCou = 0;
            cou++;
            cursor.next();
            double[] inp = new double[kol + 1];
            int j = 0;
            for (String vec : ks) {

                if (cursor.curr().containsField(vec)) {
                    inp[j] = -(double) (Double) cursor.curr().get(vec);

                } else {
                    inp[j] = 0.0;
                    zeroCou++;
                }
                j++;
            }

            if (cursor.curr().containsField(key)) {
                inp[j] = -(double) (Double) cursor.curr().get(key);
                if (inp[j] == 0) {
                    zeroCou++;
                }
            } else {
                inp[j] = 0.0;
            }

            if (!allZero || zeroCou < kol) {
                vyb.add(inp);
            }
            if (zeroCou == kol) {
                allZero = true;
            }
        }
        cursor.close();


        Perceptron p = new Perceptron(kol, 1);
        double[][] vybMas = new double[vyb.size()][];
        vybMas = vyb.toArray(vybMas);
        double[][] trueMas = new double[vybMas.length][];
        for (i = 0; i < vybMas.length; i++) {
            for (int j = 0; j < kol; j++) {
                vybMas[i][j] = vybMas[i][j];
            }

            trueMas[i] = new double[1];
            trueMas[i][0] = vybMas[i][kol];
        }

        int wrong = 0;
        for (i = 0; i < vybMas.length; i++) {
            double[] mas = new double[kol];
            for (int k = 0; k < kol; k++) {
                mas[k] = vybMas[i][k];
            }
            double minD = Double.MAX_VALUE;
            int minInd = -1;
            double tr = -1;
            int rescou = 0;
            for (int j = 0; j < vybMas.length; j++) {
                if (i != j) {
                    double[] mas2 = new double[kol];
                    for (int k = 0; k < kol; k++) {
                        mas2[k] = vybMas[i][k];
                    }
                    double d = dist(mas, mas2);
                    if (d < minD) {
                        minInd = j;
                        minD = d;
                        tr = vybMas[j][kol];
                        rescou = 1;
                    } else {
                        tr += vybMas[j][kol];
                        rescou++;
                    }
                }
            }
            double res =
                    Math.abs(vybMas[i][kol] - tr / rescou);
            System.out.println(res);
            if (res > 40) {
                wrong++;
                for (int k = 0; k < kol + 1; k++) {
                    System.out.print(vybMas[i][k]);
                    System.out.print(" ");
                }
                System.out.println();
            }

            //trueMas[i] = new double[1];
            //trueMas[i][0] = vybMas[i][kol];
        }

        System.out.println("!");
        System.out.println(wrong);
    }

    public static double dist(double[] d1, double[] d2) {
        double res = 0;
        for (int i = 0; i < d1.length; i++) {
            res += (d1[i] - d2[i]) * (d1[i] - d2[i]);
        }
        return res;
    }


    public static void proc3(Iterable<Rhory> ic, Jongo jongo, String key) {
        int i = 0;
        Vector<String> ks = new Vector<String>();
        int kol = 5;
        for (Rhory rhor : ic) {
            String tempRes;
            if (rhor._id.z1.equals(key)) {
                tempRes = rhor._id.z2;
            } else {
                tempRes = rhor._id.z1;
            }
            if (i > 0)
                ks.add(tempRes);
            i++;
            if (i >= kol) {
                break;
            }
        }
        kol = kol - 1;
//	MongoCollection friends = jongo.getCollection("WiFiPatterns");
//	friends.find("{'00:19:2f:32:af':{$exists:true}}").;
        DBCollection col = jongo.getDatabase().getCollection("WiFiPatterns");
        BasicDBObject qb = new BasicDBObject();
        //qb.put(key,new BasicDBObject("$exists", true));
        DBCursor cursor = col.find(qb);
        Vector<double[]> vyb = new Vector<double[]>();
        int cou = 0;
        int couNew = 0;

        boolean allZero = false;

        while (cursor.hasNext()) {
            int zeroCou = 0;
            cou++;
            cursor.next();
            double[] inp = new double[kol + 1];
            int j = 0;
            for (String vec : ks) {

                if (cursor.curr().containsField(vec)) {
                    inp[j] = -(double) (Double) cursor.curr().get(vec);

                } else {
                    inp[j] = 0.0;
                    zeroCou++;
                }
                j++;
            }

            if (cursor.curr().containsField(key)) {
                inp[j] = -(double) (Double) cursor.curr().get(key);
                if (inp[j] == 0) {
                    zeroCou++;
                }
            } else {
                inp[j] = 0.0;
            }

            if (!allZero || zeroCou < kol) {
                vyb.add(inp);
            }
            if (zeroCou == kol) {
                allZero = true;
            }
        }
        cursor.close();


        Perceptron p = new Perceptron(kol, 1);
        double[][] vybMas = new double[vyb.size()][];
        vybMas = vyb.toArray(vybMas);
        double[][] trueMas = new double[vybMas.length][];
        for (i = 0; i < vybMas.length; i++) {
            for (int j = 0; j < kol; j++) {
                vybMas[i][j] = vybMas[i][j] / 100.0;
            }

            trueMas[i] = new double[1];
            trueMas[i][0] = vybMas[i][kol] / 100.0;
        }

        double res = p.Learn((vybMas), trueMas, 40);
        //System.out.println("!");
        //System.out.println(res);

    }
}
