package com.opcheese.neural;

import org.bson.types.ObjectId;

import java.util.Random;

public class Perceptron {
    private ObjectId _id;
    public double[] inLayer;
    public double[] outLayer;
    double[][] weights;
    int numInp;
    int numOut;
    public double speed = 0.2;
    public int magicNumberFunction = 0;

    public Perceptron(int numInp, int numOut) {
        inLayer = new double[numInp + 1];
        outLayer = new double[numOut];
        weights = new double[numInp + 1][];
        this.numInp = numInp + 1;
        this.numOut = numOut;
        Random r = new Random();

        for (int i = 0; i < numInp + 1; i++) {
            weights[i] = new double[numOut];
            for (int j = 0; j < numOut; j++) {
                weights[i][j] = r.nextDouble() / 5.0 + 0.1;
            }
        }
        this.inLayer[numInp] = 1.0;
    }

    public void DoInput(double[] inp) {
        for (int i = 0; i < numInp - 1; i++) {
            this.inLayer[i] = inp[i];
        }

    }

    public void Work() {
        for (int i = 0; i < numOut; i++) {
            double sum = 0;
            for (int j = 0; j < numInp; j++) {
                sum += weights[j][i] * inLayer[i];
            }
            if (magicNumberFunction == 0) {
                outLayer[i] = sum;
            } else {
                if (magicNumberFunction == 1) {
                    outLayer[i] = 1.0 / (1 + Math.exp(-sum));
                }
            }
        }
    }

    public double Learn(double[][] inp, double[][] trueOut, int epochCount) {
        double[][] dw;
        double erro = 0;
        int erroCou = 0;
        dw = new double[numInp][];


        for (int eCou = 0; eCou < epochCount; eCou++) {
            System.out.println(erro);
            erro = 0;

            erroCou = 0;
            for (int i = 0; i < numInp; i++) {
                dw[i] = new double[numOut];

                for (int j = 0; j < numOut; j++) {
                    dw[i][j] = 0.0;
                }
            }
            for (int vCou = 0; vCou < inp.length; vCou++) {
                DoInput(inp[vCou]);
                Work();

                double[] omega = new double[numOut];

                for (int j = 0; j < numOut; j++) {
                    omega[j] = trueOut[vCou][j] - outLayer[j];
                    erro += Math.abs(omega[j]);

                    if (omega[j] > 0.4) {
                        for (int ii = 0; ii < numInp; ii++) {
                            System.out.print(inp[vCou][ii]);
                            System.out.print(" ");
                        }
                        System.out.println();
                        System.out.println(omega[j]);
                        erroCou++;
                    }
                }

                if (magicNumberFunction == 1) {
                    for (int j = 0; j < numOut; j++) {
                        omega[j] *= outLayer[j] * (1.0 - outLayer[j]);
                    }
                }

                for (int i = 0; i < numInp; i++) {

                    for (int j = 0; j < numOut; j++) {

                        dw[i][j] += speed * omega[j] * inLayer[i];
                    }
                }
            }
            for (int i = 0; i < numInp; i++) {
                for (int j = 0; j < numOut; j++) {
                    weights[i][j] += dw[i][j] / inp.length;
                }
            }
        }
        return erroCou;
    }

}
