package com.opcheese.neural;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.HashSet;

/**
 * Created with IntelliJ IDEA.
 * User: nopcheese
 * Date: 18.09.12
 * Time: 22:02
 * To change this template use File | Settings | File Templates.
 */
public class CandidateSet {
    //public ArrayList<MetaStat> key1 = new ArrayList<MetaStat>();
    public static ArrayList<String> names = new ArrayList<String>();
    public static long countNames = 15;
    public BitSet bs = new BitSet((int)countNames);
    public double Support = 0;
    public boolean newOne=false;

    public static    CandidateSet JoinSets (CandidateSet cs1,CandidateSet cs2 )
    {
        CandidateSet csres = new CandidateSet();
        csres.bs =(BitSet) cs1.bs.clone();

        csres.bs.or(cs2.bs);


        return csres;
    }

    public String toString()
    {
        String res = "";
        for (int i = bs.length(); (i = bs.previousSetBit(i-1)) >= 0; ) {
            res+=names.get(i)+" | ";
        }
        res+=":"+((Double)Support).toString();
        return res;
    }

    public ArrayList<String> GetNames()
    {
        ArrayList<String> res =new ArrayList<String>(   );
        for (int i = bs.length(); (i = bs.previousSetBit(i-1)) >= 0; ) {
            res.add(names.get(i));
        }
        return res;
    }

}
